package com.example.graalrest.constant;

public class Constants {
    public static final String TASK_FINISHED = "Task has been successfully finished";
    public static final String TASK_CANCELED = "Task has been cancelled";
    public static final String TASK_IN_PROGRESS = "Task is in progress";
    public static final String TASK_IN_QUEUE = "Task is in queue";
    public static final String TASK_ERROR = "Error during the task execution";

    public static final String THERE_IS_NO_SUCH_TASK_MESSAGE = "There is no task with such Id";
    public static final String THERE_IS_NO_SUCH_TASK_CODE = "task_does_not_exist";
}
