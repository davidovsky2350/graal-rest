package com.example.graalrest.constant;

public final class SwaggerSpecConstant {
    public static final String EXECUTE_TASK = "Adds task to the list adn starts it's processing";
    public static final String GET_TASKS = "Gets the list of all tasks";
    public static final String GET_TASK_BY_ID = "Gets task by its id";
    public static final String CANCEL_TASK = "Cancels task with specific id";
    public static final String REMOVE_FINISHED_TASKS = "Removes tasks that were finished successfully";
}
