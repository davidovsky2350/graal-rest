package com.example.graalrest.service;

import com.example.graalrest.entity.GraalTask;
import com.example.graalrest.exception.NoSuchTaskException;
import com.example.graalrest.multithreading.RunnableGraalTask;
import org.graalvm.polyglot.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

import static com.example.graalrest.constant.Constants.TASK_CANCELED;
import static com.example.graalrest.constant.Constants.TASK_FINISHED;
import static com.example.graalrest.constant.Constants.TASK_IN_QUEUE;
import static com.example.graalrest.constant.Constants.THERE_IS_NO_SUCH_TASK_MESSAGE;

@Service
public class GraalServiceImpl implements GraalService {
    private Map<String, GraalTask> graalTasks = new HashMap<>();
    @Autowired
    private ExecutorService executorService;

    @Override
    public GraalTask processTask(String task) {
        GraalTask graalTask = createGraalTask();
        runTaskInSubThread(task, graalTask);
        return graalTask;
    }

    @Override
    public Collection<GraalTask> getTasks() {
        return graalTasks.values();
    }

    @Override
    public GraalTask getTaskById(String taskId) {
        Optional<GraalTask> graalTask = Optional.ofNullable(graalTasks.get(taskId));
        return graalTask.orElseThrow(() -> new NoSuchTaskException(THERE_IS_NO_SUCH_TASK_MESSAGE));
    }

    @Override
    public void cancelTask(String taskId) {
        GraalTask graalTask = graalTasks.get(taskId);
        graalTask.getContext().close(true);
        graalTask.setStatus(TASK_CANCELED);
    }

    @Override
    public void removeFinishedTasks() {
        graalTasks.entrySet().removeIf(entry -> entry.getValue().getStatus().equals(TASK_FINISHED));
    }

    private GraalTask createGraalTask() {
        UUID uuid = UUID.randomUUID();
        OutputStream outputStream = new ByteArrayOutputStream();
        GraalTask graalTask = new GraalTask();
        graalTask.setId(uuid.toString());
        Context context = Context.newBuilder()
                .out(outputStream)
                .build();
        graalTask.setContext(context);
        graalTask.setConsoleOutput(outputStream);
        graalTask.setStatus(TASK_IN_QUEUE);
        graalTasks.put(graalTask.getId(), graalTask);
        return graalTask;
    }

    private void runTaskInSubThread(String task, GraalTask graalTask) {
        RunnableGraalTask runnableGraalTask = new RunnableGraalTask(graalTask, task);
        executorService.execute(runnableGraalTask);
    }
}
