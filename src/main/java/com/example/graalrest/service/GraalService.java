package com.example.graalrest.service;

import com.example.graalrest.entity.GraalTask;

import java.util.Collection;

public interface GraalService {

    GraalTask processTask(String task);

    Collection<GraalTask> getTasks();

    GraalTask getTaskById(String taskId);

    void cancelTask(String taskId);

    void removeFinishedTasks();
}
