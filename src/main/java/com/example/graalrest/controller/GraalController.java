package com.example.graalrest.controller;

import com.example.graalrest.entity.GraalTask;
import com.example.graalrest.exception.NoSuchTaskException;
import com.example.graalrest.request.TaskRequest;
import com.example.graalrest.response.ErrorResponse;
import com.example.graalrest.response.GetTaskByIdResponse;
import com.example.graalrest.response.GetTasksResponse;
import com.example.graalrest.response.TaskResponse;
import com.example.graalrest.service.GraalService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.example.graalrest.constant.Constants.THERE_IS_NO_SUCH_TASK_CODE;
import static com.example.graalrest.constant.SwaggerSpecConstant.CANCEL_TASK;
import static com.example.graalrest.constant.SwaggerSpecConstant.EXECUTE_TASK;
import static com.example.graalrest.constant.SwaggerSpecConstant.GET_TASKS;
import static com.example.graalrest.constant.SwaggerSpecConstant.GET_TASK_BY_ID;
import static com.example.graalrest.constant.SwaggerSpecConstant.REMOVE_FINISHED_TASKS;

@RestController
public class GraalController {
    private static final Logger LOG = LogManager.getLogger(GraalController.class.getName());

    @Autowired
    private GraalService graalService;

    @PostMapping("/tasks")
    @ApiOperation(value = EXECUTE_TASK, response = TaskResponse.class)
    public TaskResponse addTask(@RequestBody TaskRequest taskRequest) {
        LOG.info("Serving POST request");
        String task = taskRequest.getTask();
        GraalTask graalTask = graalService.processTask(task);
        return new TaskResponse(graalTask.getId());
    }

    @GetMapping("/tasks")
    @ApiOperation(value = GET_TASKS, response = GetTasksResponse.class)
    public GetTasksResponse getTasks() {
        LOG.info("Serving GET request");
        return new GetTasksResponse(graalService.getTasks());
    }

    @GetMapping("/tasks/{taskId}")
    @ApiOperation(value = GET_TASK_BY_ID, response = GetTaskByIdResponse.class)
    public GetTaskByIdResponse getTaskById(@PathVariable("taskId") String taskId) {
        LOG.info("Serving GET request");
        return getGetTaskByIdResponseFromGraalTask(graalService.getTaskById(taskId));
    }

    private GetTaskByIdResponse getGetTaskByIdResponseFromGraalTask(GraalTask graalTask) {
        return new GetTaskByIdResponse(graalTask.getId(), graalTask.getStatus(),
                graalTask.getConsoleOutput().toString());
    }

    @PutMapping("/tasks/{taskId}")
    @ApiOperation(value = CANCEL_TASK)
    public void removeTask(@PathVariable("taskId") String taskId) {
        LOG.info("Serving PUT request");
        graalService.cancelTask(taskId);
    }

    @DeleteMapping("/tasks")
    @ApiOperation(value = REMOVE_FINISHED_TASKS)
    public void removeFinishedTask() {
        LOG.info("Serving DELETE request");
        graalService.removeFinishedTasks();
    }

    @ExceptionHandler({NoSuchTaskException.class})
    public ResponseEntity<ErrorResponse> handleException(NoSuchTaskException exception) {
        ErrorResponse errorResponse = new ErrorResponse(THERE_IS_NO_SUCH_TASK_CODE, exception.getMessage());
        return new ResponseEntity<ErrorResponse>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
