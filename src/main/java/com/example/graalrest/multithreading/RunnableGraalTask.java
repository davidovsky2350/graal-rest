package com.example.graalrest.multithreading;

import com.example.graalrest.entity.GraalTask;
import lombok.AllArgsConstructor;

import static com.example.graalrest.constant.Constants.TASK_ERROR;
import static com.example.graalrest.constant.Constants.TASK_FINISHED;
import static com.example.graalrest.constant.Constants.TASK_IN_PROGRESS;

@AllArgsConstructor
public class RunnableGraalTask implements Runnable {
    private static final String JS = "js";

    private GraalTask graalTask;
    private String task;

    @Override
    public void run() {
        try {
            graalTask.setStatus(TASK_IN_PROGRESS);
            graalTask.getContext().eval(JS, task);
            graalTask.setStatus(TASK_FINISHED);
        } catch (Exception e) {
            graalTask.setStatus(TASK_ERROR);
        }
    }
}
