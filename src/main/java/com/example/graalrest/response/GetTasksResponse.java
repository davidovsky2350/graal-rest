package com.example.graalrest.response;

import com.example.graalrest.entity.GraalTask;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetTasksResponse {
    Collection<GraalTask> tasks;
}
