package com.example.graalrest.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetTaskByIdResponse {
    private String id;
    private String status;
    private String consoleOutput;
}
