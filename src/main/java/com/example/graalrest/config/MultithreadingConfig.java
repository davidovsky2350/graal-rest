package com.example.graalrest.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class MultithreadingConfig {
    @Value("${spring.config.threadpool.size}")
    private Integer threadPoolSize;

    @Bean
    ExecutorService executorService() {
        return Executors.newFixedThreadPool(threadPoolSize);
    }
}
