package com.example.graalrest.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.graalvm.polyglot.Context;

import java.io.OutputStream;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GraalTask {
    private String id;
    @JsonIgnore
    private Context context;
    private String status;
    @JsonIgnore
    private OutputStream consoleOutput;
}
