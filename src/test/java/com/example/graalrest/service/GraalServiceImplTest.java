package com.example.graalrest.service;

import com.example.graalrest.entity.GraalTask;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static com.example.graalrest.constant.Constants.TASK_CANCELED;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class GraalServiceImplTest {

    @Autowired
    private GraalService graalService;

    private String smallJSTask = "console.log(1)";
    private String infiniteJSTask = "for (var i=0;i>1;i--) { }";
    private int smallJSTasksCount = 3;

    @Test
    void processTask() throws InterruptedException {
        GraalTask graalTask = graalService.processTask(smallJSTask);
        assertNotNull(graalTask);
    }

    @Test
    void getTasks() {
        for (int i = 0; i < smallJSTasksCount; i++) {
            graalService.processTask(smallJSTask);
        }
        assertEquals(smallJSTasksCount, graalService.getTasks().size());
    }

    @Test
    void getTaskById() {
        GraalTask graalTask = graalService.processTask(smallJSTask);
        String taskId = graalTask.getId();
        assertEquals(graalTask, graalService.getTaskById(taskId));
    }

    @Test
    void cancelTask() {
        GraalTask graalTask = graalService.processTask(infiniteJSTask);
        String taskId = graalTask.getId();
        graalService.cancelTask(taskId);
        graalTask = graalService.getTaskById(taskId);
        assertEquals(TASK_CANCELED, graalTask.getStatus());
    }

    @Test
    void removeFinishedTasks() throws InterruptedException {
        int smallJSTaskExecutionTime = 1000;
        for (int i = 0; i < smallJSTasksCount; i++) {
            graalService.processTask(smallJSTask);
            Thread.sleep(smallJSTaskExecutionTime);
        }
        graalService.processTask(infiniteJSTask);
        assertEquals(smallJSTasksCount + 1, graalService.getTasks().size());
        graalService.removeFinishedTasks();
        assertEquals(1, graalService.getTasks().size());
    }
}